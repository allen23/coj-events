/* *********************************************************
 * ********************************************************
 * *
 * *                    LAYOUT EFFECTS
 * *
 * *******************************************************
 * ********************************************************/

function useResponsiveLayout (store) {

    const [updateImageSizes, updateColHeights] = useResponsiveImageSize(store);
    const [initialBreakpoint, updateBreakpoint] = useBreakpoints(store, updateImageSizes);

    $(window).on('resize', () => {
        updateBreakpoint();
        updateImageSizes();
    })

    return [initialBreakpoint(), updateImageSizes, updateColHeights, updateBreakpoint];
}


function useResponsiveImageSize (store) {
    const s = store.state;
   /*
    *  Maintain appropriate size, shape, and position of masonry
    *  elements when user resizes the window.
    */
    function updateImageSizes() {
         $(".grid-brick img").css({ width: "100%" })
         $(".grid-brick a").css({ width: "100%" })
         $(".gridcol").css({ "align-items": "flex-start" });

         updateColHeights()
    };

    function updateColHeights () {
        s.$masonryContainer.css('height', Math.max(...s.colHeights))
        s.colHeights = newColHeights();
    }

    function newColHeights () {
        return s.gridCols.map(
            ($col) => $col.find('.grid-sizer')
                            .toArray()
                                .reduce((acc, gridItem) =>
                                        acc + $(gridItem).height()
                                            + s.layout.VERTICAL_PADDING_PER_IMAGE
                                , 0)
        );
    }

    return [updateImageSizes, updateColHeights]
}

/* Responsive number of columns */

function useBreakpoints (store) {
    /*
     * Configure window resize handler
     */
    function updateBreakpoint(shouldDo){
        const s = store.state
        const breakpoint = getCurrentBreakpoint()

        let breakpointChanges = shouldDo || (breakpoint.minWidth !== s.breakpoint.minWidth);

        if (breakpointChanges) {
            s.breakpoint = breakpoint;

            const { nCols } = s.breakpoint,
                  newCols = s.allCols.slice(0, nCols),
                  newColHeights = newCols.map(_ => 0);

            s.allCols.forEach($col => $col.remove())

            s.gridCols = newCols;
            s.colHeights = newColHeights;

            s.gridCols.forEach($col => $col.appendTo(s.$masonryContainer))

            // effectfully redistribute images
            s.curImages.forEach(img => {
                const colIndex = s.colHeights.indexOf(Math.min(...s.colHeights)),
                      $img = $(img)
                      $gridItem = $img.closest('.grid-sizer');

                $gridItem.detach();
                s.gridCols[colIndex].append($gridItem);
                s.colHeights[colIndex] += ($gridItem.height()) + s.layout.VERTICAL_PADDING_PER_IMAGE

            })

        }

    }
    /*
     * Utility functions
     */

    function getCurrentBreakpoint () {
        return store.config.breakpoints.reduce(function (acc, x) {
         /*
          * Algorithm:
          *   find the breakpoint that
          *      (a) has the largest number of columns
          *      (b) has a `minWidth` that is smaller than the current screen size
          *      (c) has a `minWidth` property that is the smallest compared to other possible breakpoints
          */
            const a = x.nCols > acc.nCols,
                  b = x.minWidth < window.innerWidth,
                  c = x.minWidth > acc.minWidth;
            return (a && b && c) ? x : acc

         }, store.utils.createBreakpoint('', 0, 0))
    }
    //Return the current breakpoint
     return [getCurrentBreakpoint, updateBreakpoint]
}



/* *********************************************************
 * ********************************************************
 * *
 * *                      STATE STORE
 * *
 * *******************************************************
 * ********************************************************/


function useStore({
    $images,
    initialBreakpoint,
    gridCols,
    allCols,
    curImages,
    $masonryContainer
}) {

    /* UTILITY FUNCTIONS */

    function createBreakpoint (id, minWidth, nCols) {
        return ({
            id: id,
            minWidth: minWidth,
            nCols: nCols
        })
    }

    function createMasonryItem (img_ref) {
        let $gridSizer = $('<div/>', {class:'grid-sizer'}),
            $gridBrick = $('<div/>', {class:'grid-brick'}),
            $lightboxLink = $(`<a href=${img_ref.src}></a>`).append($(img_ref))


        return $gridSizer
                .append($gridBrick
                    .append($lightboxLink)
                );
    }

    return ({
        state: {
            // image elements
            n_load_events: 0,
            $images: $images,
            curImages: curImages,

            $masonryContainer: $masonryContainer,

            // column sizing
            maxCols: 3,
            colHeights: gridCols.map(_ => 0),
            gridCols: [...gridCols],
            allCols: [...allCols],

            // layout
            breakpoint: initialBreakpoint,
            layout: {
                VERTICAL_PADDING_PER_IMAGE: 16
            }
        },
        config: {
            breakpoints: [
                createBreakpoint('lg', 960, 3),
                createBreakpoint('md', 720, 2),
                createBreakpoint('sm', 500, 1),
                createBreakpoint('xs', 1, 1),
            ]
        },
        utils: {
            createBreakpoint,
            createMasonryItem
        }
    })
}



/* *********************************************************
 * ********************************************************
 * *
 * *                    MASONRY
 * *
 * *******************************************************
 * ********************************************************/

function useMasonry() {
    /*
    *  1. Use jQuery to get references to all DOM elements of interest
    */
    let $masonryContainer = $('.portfolio-scroll-container').children('.masonry'),
    allCols = [
        $('<div/>', {class:'gridcol'}),
        $('<div/>', {class:'gridcol'}),
        $('<div/>', {class:'gridcol'})
    ],
    $images = $('.portfolio-image-loader').find('img');
    /*
    *  2. Configure the store & effect hooks
    */
    const store = useStore({
        $images: $images,
        gridCols: allCols,
        allCols: allCols,
        curImages: [],
        $masonryContainer: $masonryContainer
    })

    const [
        initialBreakpoint,
        updateImageSizes,
        updateColHeights,
        updateBreakpoint,
    ] = useResponsiveLayout(store);

    const s = store.state;

    s.breakpoint = initialBreakpoint;
    s.gridCols = s.gridCols.slice(0, s.breakpoint.nCols);
    s.colHeights = s.gridCols.map(_ => 0);

    s.gridCols.map($col => {
        $masonryContainer.append($col)
    })
    /*
    *  3. Setup a pipe that consructs the masonry
    */
    const { scan, filter, map, tap } = rxjs.operators;

    const subject = new rxjs.Subject();

    const masonry_construction_pipe = subject.pipe(
        // scan and filter all incoming onLoad events to ensure that each image is only loaded once
        map((img) => {
            const s = store.state;

            let alreadyLoaded = s.curImages.map(x => x.src).includes(img.src),
                allImagesLoaded = s.curImages.length >= s.$images.length;

            return (alreadyLoaded || allImagesLoaded) ? null : img
        }),
        filter(img => img),

        // tap the pipe to perform rendering of the image masonry
        tap(curImage => {
            const s = store.state,
                colIndex = s.colHeights.indexOf(Math.min(...s.colHeights));

            let $col = s.gridCols[colIndex],
                $newMasonryItem = store.utils.createMasonryItem(curImage),
                $curImage = $(curImage);

            // add the image to the masonry layout with fade-in effect & adjust container height
            $col.append($newMasonryItem);
            $curImage.hide().fadeIn(1000);

            // update state
            s.colHeights[colIndex] += $newMasonryItem.height()+store.state.layout.VERTICAL_PADDING_PER_IMAGE;
            s.curImages = s.curImages.concat([curImage]);

            s.$masonryContainer.css('height', Math.max(...s.colHeights));
            s.$images = $('.portfolio-image-loader, .portfolio-scroll-container').find('img');
        })
    );

    // subscription triggers rendering
    masonry_construction_pipe.subscribe(() => {console.log("image loaded")});

    /*
    *  4. Update masonry container on future image loads
    */
    const sbj = new rxjs.Subject();

    const masonry_update_pipe = sbj.pipe(
        map(x => x),
        tap(() => {
            updateImageSizes()
            updateBreakpoint(true)
            store.state.n_load_events +=1
        })
    );

    // subscription triggers rendering
    masonry_update_pipe.subscribe(() => {console.log("image load")});


    /*
    *  5. Loop to ensure normal layout as images load
    *
    */

    let nTicks = 0;
    function layout_normalization_loop(){
        const s = store.state,
            STOP = nTicks >= 40;

        console.log('norm', s.n_load_events, s.curImages.length)
        let allImagesLoaded = (s.n_load_events === s.curImages.length);

        if (allImagesLoaded || STOP) return;

        updateImageSizes()
        updateBreakpoint(true)

        setTimeout(() => layout_normalization_loop(), 250);
        nTicks+=1;
    }


   /*
    *  6. Make it go
    *
    */
    layout_normalization_loop()
    s.$images.toArray().forEach($img => masonry_construction_pipe.next($img));
    s.$images.on('load', (event) => masonry_update_pipe.next(event));
}

function useGallery () {
    /*
    *   To use the gallery, include the following two HTML elements in your code:
    *
    *
    *   <div class='portfolio-image-loader'>
    *      <!-- LOAD IMAGES (with `display:none`) HERE -->
    *   </div>
    *
    *
    *   <div class='portfolio-scroll-container'>
    *      <div class='masonry gallery'>
    *         <!-- THIS IS WHERE IMAGES APPEAR -->
    *      </div>
    *   </div>
    *
    */

    useMasonry()

    const lightbox = new SimpleLightbox('.gallery a', {
        animationSlide: false,
        animationSpeed: 0
    });
}