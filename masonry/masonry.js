/*
 *  1. Use jQuery to get references to all DOM elements of interest
 */
        let $masonryContainer = $('.portfolio-scroll-container').children('.masonry'),
            allCols = [
                $('<div/>', {class:'gridcol'}),
                $('<div/>', {class:'gridcol'}),
                $('<div/>', {class:'gridcol'})
            ],
            $images = $('.portfolio-image-loader').find('img');
/*
 *  2. Configure the store & effect hooks
 */
        const store = useStore({
            $images: $images,
            gridCols: allCols,
            allCols: allCols,
            curImages: [],
            $masonryContainer: $masonryContainer
        })

        const [
            initialBreakpoint,
            updateImageSizes,
            updateColHeights,
            updateBreakpoint,
        ] = useResponsiveMasonry(store);

        const s = store.state;

        s.breakpoint = initialBreakpoint;
        s.gridCols = s.gridCols.slice(0, s.breakpoint.nCols);
        s.colHeights = s.gridCols.map(_ => 0);

        s.gridCols.map($col => {
            $masonryContainer.append($col)
        })
/*
 *  3. Setup a pipe that consructs the masonry
 */
        const { scan, filter, map, tap } = rxjs.operators;

        const subject = new rxjs.Subject();

        const masonry_construction_pipe = subject.pipe(
            // scan and filter all incoming onLoad events to ensure that each image is only loaded once
            map((img) => {
                const s = store.state;

                let alreadyLoaded = s.curImages.map(x => x.src).includes(img.src),
                    allImagesLoaded = s.curImages.length >= s.$images.length;

                return (alreadyLoaded || allImagesLoaded) ? null : img
            }),
            filter(img => img),

            // tap the pipe to perform rendering of the image masonry
            tap(curImage => {
                const s = store.state,
                      colIndex = s.colHeights.indexOf(Math.min(...s.colHeights));

                let $col = s.gridCols[colIndex],
                    $newMasonryItem = store.utils.createMasonryItem(curImage),
                    $curImage = $(curImage);

                // add the image to the masonry layout with fade-in effect & adjust container height
                $col.append($newMasonryItem);
                $curImage.hide().fadeIn(1000);

                // update state
                s.colHeights[colIndex] += $newMasonryItem.height()+store.state.layout.VERTICAL_PADDING_PER_IMAGE;
                s.curImages = s.curImages.concat([curImage]);

                s.$masonryContainer.css('height', Math.max(...s.colHeights));
                s.$images = $('.portfolio-image-loader, .portfolio-scroll-container').find('img');
            })
        );

        // subscription triggers rendering
        masonry_construction_pipe.subscribe(() => {console.log("image loaded")});

/*
 *  4. Update masonry container on future image loads
 */
        const sbj = new rxjs.Subject();
        const masonry_update_pipe = sbj.pipe(
            map(x => x),
            tap(() => {
                updateImageSizes()
                updateBreakpoint(true)
                store.state.n_load_events +=1
            })
        );

        // subscription triggers rendering
        masonry_update_pipe.subscribe(() => {console.log("image load")});


/*
 *  5. Loop to ensure normal layout as images load
 *
 */

        let nTicks = 0;
        function layout_normalization_loop(){
            const s = store.state,
                  STOP = nTicks >= 40;

            console.log('norm', s.n_load_events, s.curImages.length)
            let allImagesLoaded = (s.n_load_events === s.curImages.length);

            if (allImagesLoaded || STOP) return;

            updateImageSizes()
            updateBreakpoint(true)

            setTimeout(() => layout_normalization_loop(), 250);
            nTicks+=1;
        }

 /*
  *  6. The switch that connects the pipes and start the loop
  */

        function useGallery () {
            /*
             *   To use the gallery, include the following two HTML elements in your code:
             *
             *
             *   <div class='portfolio-image-loader'>
             *      <!-- LOAD IMAGES (with `display:none`) HERE -->
             *   </div>
             *
             *
             *   <div class='portfolio-scroll-container'>
             *      <div class='masonry gallery'>
             *         <!-- THIS IS WHERE IMAGES APPEAR -->
             *      </div>
             *   </div>
             *
             */

            s.$images.toArray().forEach($img => masonry_construction_pipe.next($img));
            s.$images.on('load', (event) => masonry_update_pipe.next(event));
            layout_normalization_loop()

            const lightbox = new SimpleLightbox('.gallery a', {
                animationSlide: false,
                animationSpeed: 0
            });
        }