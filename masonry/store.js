
function useStore({
    $images,
    initialBreakpoint,
    gridCols,
    allCols,
    curImages,
    $masonryContainer
}) {
    return ({
        state: {
            // image elements
            n_load_events: 0,
            $images: $images,
            curImages: curImages,

            $masonryContainer: $masonryContainer,

            // column sizing
            maxCols: 3,
            colHeights: gridCols.map(_ => 0),
            gridCols: [...gridCols],
            allCols: [...allCols],

            // layout
            breakpoint: initialBreakpoint,
            layout: {
                VERTICAL_PADDING_PER_IMAGE: 16
            }
        },
        config: {
            breakpoints: [
                createBreakpoint('lg', 960, 3),
                createBreakpoint('md', 720, 2),
                createBreakpoint('sm', 500, 1),
                createBreakpoint('xs', 1, 1),
            ]
        },
        utils: {
            createBreakpoint,
            createMasonryItem
        }
    })
}

/* UTILITY FUNCTIONS */

function createBreakpoint (id, minWidth, nCols) {
    return ({
        id: id,
        minWidth: minWidth,
        nCols: nCols
    })
}

function createMasonryItem (img_ref) {
    let $gridSizer = $('<div/>', {class:'grid-sizer'}),
        $gridBrick = $('<div/>', {class:'grid-brick'}),
        $lightboxLink = $(`<a href=${img_ref.src}></a>`).append($(img_ref))


    return $gridSizer
            .append($gridBrick
                .append($lightboxLink)
            );
}