


function useResponsiveMasonry (store) {

    const [updateImageSizes, updateColHeights] = useResponsiveImageSize(store);
    const [initialBreakpoint, updateBreakpoint] = useBreakpoints(store, updateImageSizes);

    $(window).on('resize', () => {
        updateBreakpoint();
        updateImageSizes();
    })

    return [initialBreakpoint(), updateImageSizes, updateColHeights, updateBreakpoint];
}


function useResponsiveImageSize (store) {
    const s = store.state;
   /*
    *  Maintain appropriate size, shape, and position of masonry
    *  elements when user resizes the window.
    */
    function updateImageSizes() {
         $(".grid-brick img").css({ width: "100%" })
         $(".grid-brick a").css({ width: "100%" })
         $(".gridcol").css({ "align-items": "flex-start" });

         updateColHeights()
    };

    function updateColHeights () {
        s.$masonryContainer.css('height', Math.max(...s.colHeights))
        s.colHeights = newColHeights();
    }

    function newColHeights () {
        return s.gridCols.map(
            ($col) => $col.find('.grid-sizer')
                            .toArray()
                                .reduce((acc, gridItem) =>
                                        acc + $(gridItem).height()
                                            + s.layout.VERTICAL_PADDING_PER_IMAGE
                                , 0)
        );
    }

    return [updateImageSizes, updateColHeights]
}

/* Responsive number of columns */

function useBreakpoints (store) {
    /*
     * Configure window resize handler
     */
    function updateBreakpoint(shouldDo){
        const s = store.state
        const breakpoint = getCurrentBreakpoint()

        let breakpointChanges = shouldDo || (breakpoint.minWidth !== s.breakpoint.minWidth);

        if (breakpointChanges) {
            s.breakpoint = breakpoint;

            const { nCols } = s.breakpoint,
                  newCols = s.allCols.slice(0, nCols),
                  newColHeights = newCols.map(_ => 0);

            s.allCols.forEach($col => $col.remove())

            s.gridCols = newCols;
            s.colHeights = newColHeights;

            s.gridCols.forEach($col => $col.appendTo(s.$masonryContainer))

            // effectfully redistribute images
            s.curImages.forEach(img => {
                const colIndex = s.colHeights.indexOf(Math.min(...s.colHeights)),
                      $img = $(img)
                      $gridItem = $img.closest('.grid-sizer');

                $gridItem.detach();
                s.gridCols[colIndex].append($gridItem);
                s.colHeights[colIndex] += ($gridItem.height()) + s.layout.VERTICAL_PADDING_PER_IMAGE

            })

        }

    }
    /*
     * Utility functions
     */

    function getCurrentBreakpoint () {
        return store.config.breakpoints.reduce(function (acc, x) {
         /*
          * Algorithm:
          *   find the breakpoint that
          *      (a) has the largest number of columns
          *      (b) has a `minWidth` that is smaller than the current screen size
          *      (c) has a `minWidth` property that is the smallest compared to other possible breakpoints
          */
            const a = x.nCols > acc.nCols,
                  b = x.minWidth < window.innerWidth,
                  c = x.minWidth > acc.minWidth;
            return (a && b && c) ? x : acc

         }, store.utils.createBreakpoint('', 0, 0))
    }
    //Return the current breakpoint
     return [getCurrentBreakpoint, updateBreakpoint]
}